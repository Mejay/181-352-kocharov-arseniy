#include "adminwindow.h"
#include "ui_adminwindow.h"
#include "database.h"
#include <QMessageBox>
#include "checkin.h"
#include "addteacher.h"
#include "changedata.h"
#include "changeprogress.h"

AdminWindow::AdminWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AdminWindow)
{
    ui->setupUi(this);

}

AdminWindow::~AdminWindow()
{
    delete ui;
}


void AdminWindow::on_AddLearner_clicked() // показывает окно добавлени ученика
{
    CheckIn wind;
    wind.setModal(true);
    wind.exec();
    show();
}

void AdminWindow::on_AddTeacher_clicked() // показывает окно добавлени учителя
{
    AddTeacher wind;
    wind.setModal(true);
    wind.exec();
    show();
}

void AdminWindow::on_SearchUsers_clicked() // поиск запрошенной информации
{

    QString info = ui->lineEdit_20->text(); // введённая инфа
    string Info = info.toStdString();

    DataBase test("Reg"); // где будет производиться поиск

    QString str;
    // преобразование найденной информации в строку
    str = QString::fromStdString(test.searchText("tel", Info));
    str = QString::fromStdString(test.searchText("name", Info));
    str = QString::fromStdString(test.searchText("surname", Info));
    str = QString::fromStdString(test.searchText("otch", Info));

    ui->textEdit_7->setText(str);

}

void AdminWindow::on_TableProgress_clicked()
{
    QString info = ui->ProgLine->text();// введённая инфа


    string Info = info.toStdString();

    Progress test("Progress"); // где будет производиться поиск

    QString str;// преобразование найденной информации в строку
    str = QString::fromStdString(test.searchProgressText("name", Info));
    str = QString::fromStdString(test.searchProgressText("surname", Info));
    str = QString::fromStdString(test.searchProgressText("otch", Info));

    ui->ProgInfo->setText(str);
}

void AdminWindow::on_Delete_clicked() // удаление по ip
{
    QString ID = ui->lineEdit_21->text();
    int id = ID.toInt();
    DataBase delID("Reg");
    delID.del(id);
}

void AdminWindow::on_DeleteProg_clicked()// удаление по ip
{
    QString ID = ui->LineProgDelete->text();
    int id = ID.toInt();
    DataBase delID("Progress");
    delID.del(id);
}

void AdminWindow::on_Change_clicked() // показывает окно со сменой данных
{
    ChangeData wind;
    wind.setModal(true);
    wind.exec();
    show();
}




void AdminWindow::on_Change_2_clicked() // показывает окно со сменой данных
{
    ChangeProgress wind;
    wind.setModal(true);
    wind.exec();
    show();
}
